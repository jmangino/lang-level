//show page on install
browser.runtime.onInstalled.addListener(async ({reason, temporary}) =>{
	//ignore dev reloads
	if(temporary)
		return;
	switch(reason){
		case 'install':
			const url = browser.runtime.getURL('views/installed.html');
			await browser.tabs.create({ url });
			break;
	}
});

browser.tabs.onUpdated.addListener((tabId, changeInfo, tab)=>{
	if(changeInfo.status !== 'complete'){
		return;
	}
	//FIXME is this necessary? can't I use tab
	let request = browser.tabs.query({active:true, currentWindow:true});
	request.then((tabs)=>{
		addScriptToTab(tabs[0]);
	});
});

function addScriptToTab(tab){
	browser.tabs.executeScript({'file':'/js/main.js'}).catch((e)=>{
		console.error(e);	
	});
	browser.tabs.insertCSS({'file':'/styles/main.css'}).catch((e)=>{
		console.error(e);	
	});
}

let wordLists = {
	'hsk-t':[
		'/word-lists/zh/hsk-t/1.txt',
		'/word-lists/zh/hsk-t/2.txt',
		'/word-lists/zh/hsk-t/3.txt',
		'/word-lists/zh/hsk-t/4.txt',
		'/word-lists/zh/hsk-t/5.txt',
		'/word-lists/zh/hsk-t/6.txt',
	],
	'hsk-s':[
		'/word-lists/zh/hsk-s/1.txt',
		'/word-lists/zh/hsk-s/2.txt',
		'/word-lists/zh/hsk-s/3.txt',
		'/word-lists/zh/hsk-s/4.txt',
		'/word-lists/zh/hsk-s/5.txt',
		'/word-lists/zh/hsk-s/6.txt',
	],
	'tocfl':[
		'/word-lists/zh/tocfl/a1.txt',
		'/word-lists/zh/tocfl/a2.txt',
		'/word-lists/zh/tocfl/b1.txt',
		'/word-lists/zh/tocfl/b2.txt',
		'/word-lists/zh/tocfl/c1.txt',
	]
}

function urlStringToFileName(url){
	let i0 = url.lastIndexOf('/')+1;
	let i1 = url.lastIndexOf('.');
	return url.substring(i0, i1);
}

browser.runtime.onMessage.addListener(
	(data,sender) => {
		//data: {list: hsk, type:t}
		//right now I'm ignoring type if tocfl, maybe fix later
		let type = data.list==='tocfl'?'':'-'+data.type;
		browser.pageAction.show(sender.tab.id)
		let internalUrls = wordLists[data.list+type];
		Promise.all(internalUrls.map((iurl)=>{
			let filename = urlStringToFileName(iurl);
			let label = data.list+'-'+filename;
			let url = browser.extension.getURL(iurl);
			//run fetches async
			return fetch(url)
				.then(response => response.text())
				.then((text) =>{
					browser.tabs.sendMessage(sender.tab.id,
						{'label':label, 'list':text, action:'new-list'}
					);
				});	
		})).then((x)=>{
			//this message should trigger after all loading is one
			browser.tabs.sendMessage(sender.tab.id,
				{action:'make-histogram'}
			);
		})	
	}
);

