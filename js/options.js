window.onload = function(){
	let form = document.querySelector('form');
	//setup databinds
	let bind = document.querySelector('select[name=userList]')
	bind.onchange = function(event){
		form.querySelectorAll('select[for=userList]').forEach((elem)=>{
			if(elem.getAttribute('using') === event.target.value){
				elem.classList.remove('hidden');
			}else{
				elem.classList.add('hidden');
			}
		});
	}

	let config;
	let request = browser.storage.local.get('config');

	//FIXME auto form read/write doesn't handle checkboxes
	request.then((data)=>{
		config = data.config ?? {};
		for(var key in config){
			form.querySelectorAll(`input[name=${key}], select[name=${key}]`).forEach((input)=>{
				input.value = config[key];
				input.dispatchEvent(new Event('change'));
			});
		}
	});
	//after initializing all data
	form.onchange = function(){
		let submit = document.querySelector('input[type=submit]');
		submit.removeAttribute('disabled')
	}

	form.onsubmit = function(event){
		let f = event.submitter.parentElement;
		f.querySelectorAll('input, select').forEach((input)=>{
			if(!input.hasAttribute('disabled') && input.type !== 'submit' && !input.classList.contains('hidden')){
				config[input.name] = input.value;
			}	
		});
		browser.storage.local.set({config})
		event.preventDefault();
	}
}
