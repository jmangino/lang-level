window.onload = function(){

	document.querySelector('#options').onclick = function(){
		browser.runtime.openOptionsPage();
	}

	document.querySelector('#blacklist').onclick = function(){
		//FIXME if website/article1 and website/article2 -> website/*
		let config;
		let request = browser.storage.local.get('config');
		request.then((data)=>{
			config=data.config ?? {};
			if (!config.blacklist){
				config.blacklist = [];
			}	
		}).then((x)=>{
			return tabRequest = browser.tabs.query({active:true, currentWindow:true});
		}).then((tabs)=>{
			//only add if not already in there
			if(!config.blacklist.includes(tabs[0].url)){
				config.blacklist = config.blacklist.concat(tabs[0].url);
				return browser.storage.local.set({config});
			}
		}).catch((e)=>{
			console.error(e);
		});
	}
}
