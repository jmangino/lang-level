(function (){
	//run once
console.error(window.hasRun)
	if(window.hasRun){
		return;
	}
	window.hasRun = true;
//***Browser Compatibility
window.browser = window.msBrowser ?? window.browser ?? window.chrome;
const callbackFunctions = [];
//add promise support to chrome functions
//FIXME this might interfere with other extensions
//FIXME do this on a function by function basis then generalize
function overrideFunction(funcName){
	//reduce name e.g. chrome.storage.local.get
	//maybe override browser.storage.local.get (when chrome) to not mess with chrome APIs directly
	let nameList = funcName.split('.');
	let name = nameList.pop();
	let ref = nameList.reduce((a,c)=>{return a[c]},window);
	let prototype = Object.getPrototypeOf(ref);
	prototype[name]= function(...args){
		ref[name](args, function(result,error){
			return new Promise((resolve, reject)=>{
				if(error)
					reject(error);
				else
					resolve(result);	
			});
		}); 
	}
}

callbackFunctions.forEach((f)=>{
	//what if function is of type 
	Window.prototype[f.name] = function(){
		
	}
});
//***END Browser Compatibility


	//let storage = browser.storage.sync || browser.storage.local;
	let storage = browser.storage.local;

	let DEBUG = true;
	function debug(message){
		if(DEBUG){
			console.error(message);
		}
	}

	let dictionary = {};
	//determine word length for langs with no whitespace
	let trie = {};

	//FIXME these values should be encoded in the config
	const LUT = {
			'hsk-1':0,'hsk-2':1,'hsk-3':2,'hsk-4':3,'hsk-5':4,'hsk-6':5,
			'tocfl-a1':0,'tocfl-a2':1,'tocfl-b1':2,'tocfl-b2':3,'tocfl-c1':4,
	}

	function reverseLUT(value){
		return Object.entries(LUT).find(obj=>obj[0].includes(userList) &&obj[1]===value)[0];
	}
	//these wordlists are based on differences in the 600 most common Chinese characters 
	const COMMON_TRADITIONAL_REGEX = /[資這個會為來學時沒說問過請們麼還電對機後訊國發無嗎當於現點題樣經謝華話開實愛與動車種應長關鳳間覺該進著兩將龍論別給聽體裡東風灣見區錯網樂讓選較場書從歡數內認幾頭難買許記統處號師並計誰張黨結連轉報設變氣陳試戰義單臺卻隊聲寫業檔討則員興強價總辦議傳萬決貓組獨級標馬門線語觀視聯參黃錢兒腦換錄專遠幫確裝備畫訴講類帶邊識雖飛運賽費夢權驗滿軍務圖讀願約課達賣夠準談勝術離況證導傷調團剛殺絕軟條盡據雙稱產紅園雲遊執聞]/g;
	const COMMON_SIMPLIFIED_REGEX  = /[资这个会为来学时没说问过请们麽还电对机后讯国发无吗当于现点题样经谢华话开实爱与动车种应长关凤间觉该进着两将龙论别给听体里东风湾见区错网乐让选较场书从欢数内认几头难买许记统处号师并计谁张党结连转报设变气陈试战义单台却队声写业档讨则员兴强价总办议传万决猫组独级标马门线语观视联参黄钱儿脑换录专远帮确装备画诉讲类带边识虽飞运赛费梦权验满军务图读愿约课达卖够准谈胜术离况证导伤调团刚杀绝软条尽据双称产红园云游执闻]/g;

	const UNICODE_RANGES_ZH = [
		[0x4e00, 0x9fff], // CJK Unified Ideographs
	  	[0x3400, 0x4dbf], // CJK Unified Ideographs Extension A
	  	[0x20000, 0x2a6df], // CJK Unified Ideographs Extension B
	  	[0x2a700, 0x2b73f], // CJK Unified Ideographs Extension C
	  	[0x2b740, 0x2b81f], // CJK Unified Ideographs Extension D
	  	[0x2b820, 0x2ceaf], // CJK Unified Ideographs Extension E
	  	[0xf900, 0xfaff], // CJK Compatibility Ideographs
	  	[0x3300, 0x33ff], // https://en.wikipedia.org/wiki/CJK_Compatibility
	  	[0xfe30, 0xfe4f], // https://en.wikipedia.org/wiki/CJK_Compatibility_Forms
	  	[0xf900, 0xfaff], // https://en.wikipedia.org/wiki/CJK_Compatibility_Ideographs
	  	[0x2f800, 0x2fa1f], // https://en.wikipedia.org/wiki/CJK_Compatibility_Ideographs_Supplement
		
	]
	//FIXME consistency with above
	const UNICODE_REGEX_ZH = /[\u{4e00}-\u{9fff}\u{3400}-\u{4dbf}\u{20000}-\u{2a6df}\u{2a700}-\u{2b73f}\u{2b740}-\u{2b81f}\u{2b820}-\u{2ceaf}\u{f900}-\u{faff}\u{3300}-\u{33ff}\u{fe30}-\u{fe4f}\u{f900}-\u{faff}\u{2f800}-\u{2fa1f}]/gu;
	//get things like body>div>p, body>p, body>article, body>article>p
	//const TEXT_QUERY = 'body *:not(article) p, body > p, article'
	const TEXT_QUERY = 'body p, article'
	const TARGET_RATIO_ZH = 0.15;
	//FIXME change name as no longer const
	let userLevel, userList;

//*** Language Detection Helpers
	function tryIsHTMLLangTradZH(){
		let found = false, value;
		const htmlLang = document.documentElement.lang.toLowerCase();
		//try from <html lang=""> can be of form ZH-Hant-TW, HANT-TW, ZH-CN
		if(!htmlLang){
			return [found, value];
		}
		if(htmlLang.includes("hant")){
			[found, value] = [true,true];
		}
		if(htmlLang.includes("hans") || htmlLang.includes("cn")){
			[found, value] = [true,false];
		}
		return [found, value];
	}

	//FIXME it's possible the URL could have random UUID or something which triggers this detection
	function tryIsURLTradZH(){
		let found = false, value;
		const url = document.URL.toLowerCase();
		if(url.includes("zh_cn")){
			[found, value] = [true, false];
		}
		if(url.includes("zh_tw")){
			[found, value] = [true, true];
		}
		return [found, value];
	}

	function tryIsBodyTextTradZH(){
		let found = false, value;
		const trad = getCharCount(COMMON_TRADITIONAL_REGEX);
		const simp = getCharCount(COMMON_SIMPLIFIED_REGEX);
		[found, value] = [trad + simp > 0, trad > simp];
		return [found, value];
	}

//*** END Language Detection Helpers
	
	function isTraditionalZH(){
		const fallback = false;
		//try from HTML def
		let [found, value] = tryIsHTMLLangTradZH();
		//try from URL
		if(!found)
			[found, value] = tryIsURLTradZH();
		//try from body text
		if(!found)
			[found, value] = tryIsBodyTextTradZH();
		if(!found){
			debug("Could not determine lang type")
			return fallback;
		}
		//found
		return value;
	}
	
	//this currently counts whitespace and punctuation
	function getCharCount(regex){
		let count = 0;
		//loop handles multiple child p tags
		document.querySelectorAll(TEXT_QUERY).forEach((elem)=>{
			//no match regex is null -> coercing to 0
			count += regex? elem.textContent.match(regex)?.length ?? 0 : elem.textContent.length ;
		});
		return count;
	}

	function getCharCountZH(){
		return getCharCount(UNICODE_REGEX_ZH);
	}
	/*
	 * I think the regex will work better with function oriented design
	 * That said this probably has better memory perf because it is in place
	 * Instead of returning results as an array
	
	function getCharsInStringZH(str){
		let count = 0;
		for(var c of str){
			let codePoint = c.codePointAt(0);
			for (range of UNICODE_RANGES_ZH){
				if(codePoint >= range[0] && codePoint <= range[1])
					count++; 
			}
		}
		return count;
	}
*/
//***Datastructures
	function addListToDictionary(label, list, delimiter='\n'){
		if(!dictionary[label]){
			dictionary[label]={};
		}
		//trim fixes bug where CRs were corrupting object keys
		list.split(delimiter).map(w=>w.trim())
			.forEach((word)=>{
				if(word !== ''){
					//can I store this more compactly?
					dictionary[label][word]=false;
					if(word.length > 1){
						addToTrie(word);
					}	
				}
			});
	}
	
	function addToTrie(word){
		let node = trie;
		for(let i=0; i < word.length -1 ; i++){
			let char = word[i];
			if(node[char] === undefined){
				node[char] = {};
			}
			node = node[char];
		}
		let lastChar = word[word.length - 1]
		//FIXME I think this is a bug because ABC then AB -> B would lose the reference to C
		node[lastChar] = {isWord:true};
	}

	function getHistogramFromPageText(){ 
		let output = {};
		let lists = Object.keys(dictionary);
		let noMatch = 0;
		lists.forEach((key) =>{
			output[key] = 0;
		});
		document.querySelectorAll(TEXT_QUERY).forEach((elem)=>{	
			//for each character in each text section
			for(let i = 0; i < elem.textContent.length; i++){
				let word = elem.textContent[i];
				//ignore latin chars, whitespace, punctuation	
				if(!word.match(UNICODE_REGEX_ZH)){
					continue;
				}
				let current = trie[word];
				//use trie to determine if word is multi char
				for(let j=1, done=false,mcword=word; current !== undefined && !done && i+j < elem.textContent.length; j++){
					let nextChar = elem.textContent[i+j];
					//if lookup fails - next word DNE, must be current
					if(current[nextChar] === undefined){
						done = true;
						//found word in trie us multichar word, increment i
						if(current.isWord){
							word = mcword;
							i += j;							
						}	
					}else{
						//next word is valid, append
						mcword += nextChar;
						//step through trie
						current = current[nextChar];
					}		
				}
				let found = false;
				//FIXME hacky
				lists.forEach((list)=>{
					if(!found){
						found = dictionary[list][word] !== undefined;
						if(found && !dictionary[list][word]){
							//uniqueness flag
							dictionary[list][word] = true;
							output[list]++;
						}
					}
				});
				if(!found){
					noMatch ++;
//debug('unknown word:'+word+' code: '+word.codePointAt(0));
				}					
			}//for each word
		});
		return [output, noMatch];
	}
//***End Datastructures

//***Analytics
	function normFreqToLevel(freq){
		const cutoff = 0.15; //FIXME vary this number based on list size
		let max = 0;
		for(let i = 0; i < freq.length; i++){
			if(freq[i] > cutoff)
				max = i;
		}
		return max;
	}

	//FIXME
	function histToNormalizedFreq(hist){
		let total = Object.values(hist).reduce((x,y)=>x+y);
		let freq = [];
		Object.keys(hist).forEach((key)=>{
			freq[LUT[key]] = hist[key]/total;
		});
		return freq;
	}

	/**
	*Given the page histogram, calculates the toatl known and unknown words for a user
	*@param {Map} hist
	*@return {Array} [known, unknown]
	*/
	//FIXME this doesn't account for unmatched unknowns
	function getKnownUnknownWordCount(hist){
		let known = 0;
		let unknown = 0;
		//LUT is doing a string to int conversion
		let userLevelInt = LUT[userLevel];
		Object.keys(hist).forEach((level)=>{
			if(LUT[level] <= userLevelInt){
				known += hist[level];
			}else{
				unknown += hist[level];
			}
		});
		return [known, unknown]
	}
//***End Analytics	
//***Start UI
function documentHasAbsoluteHeader(){
	const elem = document.elementFromPoint(0,0);
	//elem is topmost so it might be an img with no position, but has parent with absolute or fixed pos
	let found = false;
	for(let current = elem; !found && current != document.body && current != document.body.parentElement; current = current.parentElement){
debug(current);
		//using elem.style will fail if the position is set by a class, so using getComputedStyle
		found = window.getComputedStyle(current).position.search(/fixed|absolute|sticky/) != -1;
	}
	return found;
}

function toggleFold(){
	const div=document.querySelector('div.ll-fold');
	if(div.classList.contains('ll-fold-hidden')){
		div.classList.remove('ll-fold-hidden');
	}else{
		div.classList.add('ll-fold-hidden');
	}
}

function removeUI(){
	let elem = document.querySelector('#ll-header');
	elem.parentElement.removeChild(elem);
	return elem;
}

function freeAllMemory(){
//Sonar says not to delete objects directly
	delete window.trie;
	delete window.dictionary;
}

function renderComponent(){
	//make this a webcomponent to avoid conflicts?
	const header = document.createElement('header');
	header.style="width:100%; background:#ee8;"
	header.id = "ll-header"
	header.innerHTML=`
	<button id="ll-ui-close" style='float:right;font-weight:600;padding:0px;border:none;border-bottom:1px solid #333;'>&times;</button>
	Language Level <span id="ll-level" style="color:#fff;font-weight:900;font-size:1.5em;padding:0.06em;text-transform:uppercase;"></span>
	<button id='ll-ui-toggle' style="width:100%;">more</button>
	<div class="ll-fold ll-fold-hidden">
	Debug Info: char count: <span id="ll-debug-char-count"></span>&nbsp; |
	zh-char-count: <span id="ll-debug-char-count-zh"></span>&nbsp;
	character-set: <span id="ll-debug-char-set"></span>
	</div>
	`;
	header.querySelector('#ll-ui-toggle').onclick = toggleFold;
	header.querySelector('#ll-ui-close').onclick = function(){
		removeUI();
		freeAllMemory();
	}
let absHead = documentHasAbsoluteHeader();
debug(`Absolute header? ${absHead}`)
	let count = getCharCount();
	header.querySelector('#ll-debug-char-count').textContent = count;
	let countZH = getCharCountZH();
	header.querySelector('#ll-debug-char-count-zh').textContent = countZH;
	let detectedLang = isTraditionalZH()?'T':'S';
	header.querySelector('#ll-debug-char-set').textContent=detectedLang;
	let properties = {};
	properties.list = userList; 
	properties.type = detectedLang.toLowerCase();
	let ratioZH = countZH / count;
	//FIXME consider using ratio of target ot non target ie what if only 1% target lang
	if(ratioZH > TARGET_RATIO_ZH){
		browser.runtime.sendMessage(properties);
		document.body.insertBefore(header,document.body.firstElementChild);
	}
}

//***End UI
	browser.runtime.onMessage.addListener((message)=>{
		switch(message.action){
			case 'new-list': 
				addListToDictionary(message.label, message.list);
				break;
			case 'make-histogram':
				let [hist, noMatch] = getHistogramFromPageText();
				let freq = histToNormalizedFreq(hist);
debug(freq)
				let level = reverseLUT(normFreqToLevel(freq));
				let levelElement = document.querySelector('#ll-level');
				levelElement.textContent = level;
				levelElement.classList.add(level);
				let [known, unknown] = getKnownUnknownWordCount(hist);
debug([known, unknown])
				break;
		}
	});

//***Make it all go
	let request = storage.get('config');
	let config;
	request.then((result)=>{
		if(!result.config){
			//FIXME config isn't defined prompt user
			config = {};
		}else{
			config = result.config;
		}
		//FIXME define these defaults centrally
		userList = result.config?.userList ?? 'hsk' ;
		userLevel = result.config?.userLevel ?? 'hsk-1';
		if(config.blacklist){
			return result.config.blacklist.includes(window.location.href)
		}
		return false;
	}).then((blacklist)=>{
		//nav back and forth is causing the script to rerun? Prevent double loading the UI
		if(!blacklist && !document.querySelector('#ll-header'))
			renderComponent();
	}).catch((e)=>{
		console.error(e)
	});
})();

